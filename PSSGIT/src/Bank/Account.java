package Bank;

import java.util.Random;

public class Account implements Comparable<Account>{
	
	private String iban;
	private double money;
	private User owner;
	
	public Account() {//Create a default account (0 �)
		this.iban = generateIban();
		this.money = 0;
		this.owner = null;
	}
	
	public Account(double money) {//Create an account with money
		this.iban = generateIban();
		this.money = money;
		this.owner = null;
	}
	
	public Account(String iban) {//Searchs
		this.iban = iban;
	}

	public double getMoney() {
		return money;
	}
	
	public String getIban() {
		return iban;
	}
	
	public User getOwner() {
		return owner;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

	public void addMoney(double money) throws NegativeMoneyException {
		if(money < 0) {
			throw new NegativeMoneyException("You can�t add a negative value");
		}
		//sino, le sumo el dinero que hemos puesto
		this.money += money;
	}
	//sacar dinero
	public double drawMoney(double money) throws NegativeMoneyException, ExcedeedMoneyException {
		if(money < 0) {
			throw new NegativeMoneyException("You can�t add a negative value");
		}
		//si el dinero que queremos sacar es mayor que el que tengo...
		if(money > this.money) {
			throw new ExcedeedMoneyException("You can�t draw more money than you have");
		}
		this.money -= money;
		return this.money;
	}

	@Override
	public String toString() {
		return "Account "+this.iban+(owner==null?"(Unassigned)":"("+this.owner.getName()+")")+": Total=" + money + "�";
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((iban == null) ? 0 : iban.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Account other = (Account) obj;
		if (iban == null) {
			if (other.iban != null)
				return false;
		} else if (!iban.equals(other.iban))
			return false;
		return true;
	}
	
	@Override
	public int compareTo(Account o) {
		return this.iban.compareToIgnoreCase(o.iban);
	}

	private String generateIban() {
		Random rand = new Random();
	    String card = "ES";
	    for (int i = 0; i < 14; i++){
	        int n = rand.nextInt(10) + 0;
	        if((i+2) % 4 == 0) {
	        	card += " ";
	        }
	        card += Integer.toString(n);
	    }
	    return card;
	    
	}
	
	
	
	
	
	
	

}
