package Bank;

import java.util.Comparator;

public class MultiComparator {
	
	public static final Comparator<Account> BYMONEY_LESS = new Comparator<Account>() {
		
		@Override
		public int compare(Account a1, Account a2) {
			int comp = Double.compare(a1.getMoney(), a2.getMoney());
			return comp == 0 ? a1.compareTo(a2) : comp;
		}
	};
	
	public static final Comparator<Account> BYMONEY_GREATER = new Comparator<Account>() {
		
		@Override
		public int compare(Account a1, Account a2) {
			int comp = -Double.compare(a1.getMoney(), a2.getMoney());
			return comp == 0 ? a1.compareTo(a2) : comp;
		}
	};
	
	public static final Comparator<Account> BYOWNER_LESS = new Comparator<Account>() {
		
		@Override
		public int compare(Account a1, Account a2) {
			int comp = a1.getOwner().getName().compareToIgnoreCase(a2.getOwner().getName());
			return comp == 0 ? a1.compareTo(a2) : comp;
		}
	};
	
	public static final Comparator<Account> BYOWNER_GREATER = new Comparator<Account>() {
		
		@Override
		public int compare(Account a1, Account a2) {
			int comp = -a1.getOwner().getName().compareToIgnoreCase(a2.getOwner().getName());
			return comp == 0 ? a1.compareTo(a2) : comp;
		}
	};
	
	public static final Comparator<Account> BYACCOUNTNO_LESS = new Comparator<Account>() {
		
		@Override
		public int compare(Account a1, Account a2) {
			return a1.compareTo(a2);
		}
	};
	
	public static final Comparator<Account> BYACCOUNTNO_GREATER = new Comparator<Account>() {
		
		@Override
		public int compare(Account a1, Account a2) {
			return -a1.compareTo(a2);
		}
	};
	

}
