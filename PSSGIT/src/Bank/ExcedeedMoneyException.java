package Bank;

public class ExcedeedMoneyException extends Exception {
	
	public ExcedeedMoneyException(String message) {
		super(message);
	}

}
