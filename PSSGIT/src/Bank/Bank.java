package Bank;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeSet;

public class Bank {
	
	private HashMap<User, HashSet<Account>> bank;

	public Bank() {
		this.bank = new HashMap<User, HashSet<Account>>();
	}
	
	public boolean addUser(String name) {
		if(name == null || name.trim().isEmpty()) return false;
		User u = new User(name);
		return addUser(u);
	}
	
	public boolean addUser(User u) {
		if(u == null) return false;
		if(this.bank.containsKey(u)) return false;
		this.bank.put(u, new HashSet<Account>());
		return true;
	}
	//este metodo llama al siguiente metodo
	public boolean addAccount(String username, Account a) {
		if(username == null) return false;
		User u = new User(username);
		return addAccount(u, a);
	}
	
	public boolean addAccount(User u, Account a) {
		if(u == null) return false;
		addUser(u);
		boolean unassigned = true;
		for (HashSet<Account> it : this.bank.values()) {
			if(it.contains(a)) {
				unassigned = false;
				boolean flag = false;
				for (Account account : it) {
					if(account.equals(a)) {
						a = account;
						flag = true;
						break;
					}
				}
				if(flag) {
					break;
				}
			}
		}
		if(unassigned) {
			a.setOwner(u);
		}
		return this.bank.get(u).add(a);
	}
	
	public boolean addMoney(String iban, double money) throws NegativeMoneyException {
		Account aux = new Account(iban);
		for (HashSet<Account> it : this.bank.values()) {
			for (Account a : it) {
				if(a.equals(aux)) {
					a.addMoney(money);
					return true;
				}
			}
		}
		return false;
	}
	
	public double drawMoney(String iban, double money) throws NegativeMoneyException, ExcedeedMoneyException {
		Account aux = new Account(iban);
		double moneyLeft = 0;
		for (HashSet<Account> it : this.bank.values()) {
			for (Account a : it) {
				if(a.equals(aux)) {
					moneyLeft = a.drawMoney(money);
					return moneyLeft;
				}
			}
		}
		return moneyLeft;
	}
	
	public double showMoney(String iban) {
		Account aux = new Account(iban);
		for (HashSet<Account> it : this.bank.values()) {
			for (Account a : it) {
				if(a.equals(aux)) {
					return a.getMoney();
				}
			}
		}
		throw new RuntimeException("No account with IBAN "+iban);
	}
	
	public double showSumUser(String username) {
		User aux = new User(username);
		if(!this.bank.containsKey(aux)) {
			throw new RuntimeException("No user found");
		}
		double sum = 0;
		for (Account a : this.bank.get(aux)) {
			sum += a.getMoney();
		}
		return sum;
	}
	
	public List<User> showAllUsersAccount(String iban){
		List<User> list = new ArrayList<User>();
		Account aux = new Account(iban);
		for (Entry<User, HashSet<Account>> it : this.bank.entrySet()) {
			if(it.getValue().contains(aux)) {
				list.add(it.getKey());
			}
		}
		return list;
	}
	
	public User showAccountOwner(String iban) {
		Account aux = new Account(iban);
		for (HashSet<Account> it : this.bank.values()) {
			for (Account a : it) {
				if(a.equals(aux)) {
					return a.getOwner();
				}
			}
		}
		throw new RuntimeException("No account with IBAN "+iban);
	}
	//estructura ordenada que no permite repetidos
	public TreeSet<Account> listAccounts(OrderType type, boolean asc){
		Comparator<Account> comp = null;
		switch (type) {
		case MONEY:
			comp = asc ? MultiComparator.BYMONEY_LESS : MultiComparator.BYMONEY_GREATER;
			break;
		case OWNER:
			comp = asc ? MultiComparator.BYOWNER_LESS : MultiComparator.BYOWNER_GREATER;
			break;
		case ACCOUNTNO:
			comp = asc ? MultiComparator.BYACCOUNTNO_LESS : MultiComparator.BYACCOUNTNO_GREATER;
			break;
		}
		TreeSet<Account> set = new TreeSet<Account>(comp);
		for (HashSet<Account> it : this.bank.values()) {
			set.addAll(it);
		}
		return set;
	}
	 

}
