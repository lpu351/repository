package Bank;

import java.util.Scanner;
import java.util.TreeSet;

public class MainBank {
	
	private static Bank bank;

	public static void main(String[] args) {
		bank = new Bank();
		do {
			menu();
			int option = readOption("Choose option: ", 1, 9);
			execute(option);
		} while (true);
	}
	
	private static void menu() {
		System.out.println("===================");
		System.out.println("        MENU       ");
		System.out.println("===================");
		System.out.println("1.- Add user");
		System.out.println("2.- Add account");
		System.out.println("3.- Add money to an account");
		System.out.println("4.- Draw money from an account");
		System.out.println("5.- Show money saved on an account");
		System.out.println("6.- Show sum of money on all accounts of an user");
		System.out.println("7.- Show the owner of an account");
		System.out.println("8.- List ordered accounts");
		System.out.println("9.- Exit");
	}
	
	private static int readOption(String message, int min, int max) {
		Scanner sc = new Scanner(System.in);
		while (true) {
			try {
				System.out.println(message);
				int option = Integer.parseInt(sc.nextLine());
				if(option < min || option > max) {
					System.out.println("Error: you must enter a number between "+min+" and "+max);
				}else {
					return option;
				}
			} catch (NumberFormatException e) {
				System.out.println("Error: you must enter a whole number");
			}
		}
	}
	
	private static void execute(int option) {
		switch (option) {
		case 1: addUser(); break;
		case 2: addAccount(); break;
		case 3: addMoney(); break;
		case 4: drawMoney(); break;
		case 5: showMoneyAccount(); break;
		case 6: showSumMoneyUser(); break;
		case 7: showAccountOwner(); break;
		case 8: listOrderedAccounts(); break;
		default: System.out.println("Finishing the program");
			System.exit(0);
		}
	}

	private static void listOrderedAccounts() {
		Scanner sc = new Scanner(System.in);
		OrderType ot = OrderType.MONEY;
		boolean asc = true;
		do {
			System.out.println("Select list type:\n1. By Money\n2. By Owner\n3. By Account Number");
			try {
				int option = Integer.parseInt(sc.nextLine());
				if(option < 1 || option > 3) {
					System.out.println("Error: must select an option between 1 and 3");
				}else {
					switch (option) {
					case 1: ot = OrderType.MONEY; break;
					case 2: ot = OrderType.OWNER; break;
					default: ot = OrderType.ACCOUNTNO; break;
					}
					break;
				}
			} catch (NumberFormatException e) {
				System.out.println("Error: must select a numbered option");
			}
			
		} while (true);
		
		do {
			System.out.println("Select order:\n1. Ascending\n2. Descending");
			try {
				int option = Integer.parseInt(sc.nextLine());
				if(option < 1 || option > 2) {
					System.out.println("Error: must select an option between 1 and 2");
				}else {
					switch (option) {
					case 1: asc = true; break;
					default: asc = false; break;
					}
					break;
				}
			} catch (NumberFormatException e) {
				System.out.println("Error: must select a numbered option");
			}
			
		} while (true);
		
		TreeSet<Account> set = bank.listAccounts(ot, asc);
		int i = 1;
		for (Account a : set) {
			System.out.println(i+": "+a);
			i++;
		}
		
		
	}

	private static void showAccountOwner() {
		Scanner sc = new Scanner(System.in);
		String iban = "";
		do {
			System.out.println("Enter iban: ");
			iban = sc.nextLine();
		} while (iban.trim().isEmpty());
		
		try {
			System.out.println("The account owner is "+bank.showAccountOwner(iban).getName());
		} catch (RuntimeException e) {
			System.out.println(e.getMessage());
		}
		
	}

	private static void showSumMoneyUser() {
		Scanner sc = new Scanner(System.in);
		String name = "";
		do {
			System.out.println("Enter username: ");
			name = sc.nextLine();
		} while (name.trim().isEmpty());
		
		try {
			System.out.println("This user has a total of "+bank.showSumUser(name)+"�");
		} catch (RuntimeException e) {
			System.out.println(e.getMessage());
		}
		
		
	}

	private static void showMoneyAccount() {
		Scanner sc = new Scanner(System.in);
		String iban = "";
		do {
			System.out.println("Enter iban: ");
			iban = sc.nextLine();
		} while (iban.trim().isEmpty());
		
		try {
			System.out.println("This account has "+bank.showMoney(iban)+"�");
		} catch (RuntimeException e) {
			System.out.println(e.getMessage());
		}
		
	}

	private static void drawMoney() {
		Scanner sc = new Scanner(System.in);
		String iban = "";
		double money = 0;
		do {
			System.out.println("Enter iban: ");
			iban = sc.nextLine();
		} while (iban.trim().isEmpty());
		do {
			System.out.println("Enter money to add: ");
			try {
				money = Double.parseDouble(sc.nextLine());
				break;
			} catch (NumberFormatException e) {
				System.out.println("Error: money must be a number");
			}
			
		} while (true);
		
		try {
			bank.drawMoney(iban, money);
		} catch (NegativeMoneyException e) {
			System.out.println("Error: money must be a positive value");
		} catch (ExcedeedMoneyException e) {
			System.out.println("Error: this account doesn�t have enough money");
		}
		
	}

	private static void addMoney() {
		Scanner sc = new Scanner(System.in);
		String iban = "";
		double money = 0;
		do {
			System.out.println("Enter iban: ");
			iban = sc.nextLine();
		} while (iban.trim().isEmpty());
		do {
			System.out.println("Enter money to add: ");
			try {
				money = Double.parseDouble(sc.nextLine());
				break;
			} catch (NumberFormatException e) {
				System.out.println("Error: money must be a number");
			}
			
		} while (true);
		try {
			bank.addMoney(iban, money);
		} catch (NegativeMoneyException e) {
			System.out.println("Error: money must be a positive value");
		}
		
	}

	private static void addAccount() {
		Scanner sc = new Scanner(System.in);
		String name = "";
		String iban = "";
		do {
			System.out.println("Enter the username: ");
			name = sc.nextLine();
		} while (name.trim().isEmpty());
		
		do {
			System.out.println("Enter iban: ");
			iban = sc.nextLine();
		} while (iban.trim().isEmpty());
		Account a = new Account(iban);
		bank.addAccount(name, a);
	}

	private static void addUser() {
		Scanner sc = new Scanner(System.in);
		String name = "";
		do {
			System.out.println("Enter the username: ");
			name = sc.nextLine();
		} while (name.trim().isEmpty());
		bank.addUser(name);
	}
}
