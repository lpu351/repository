package Bank;

public class NegativeMoneyException extends Exception {
	
	public NegativeMoneyException(String message) {
		super(message);
	}

}
