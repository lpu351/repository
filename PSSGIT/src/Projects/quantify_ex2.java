package Projects;
/*
 * quantify_ex2.java
 *
 * Created on 4 grudzień 2004, 21:38
 */

public class quantify_ex2 {
    
    private static int z = 10000;
    private static double d = 3.678;
    private static double lim = 2.456;
    private static double max = 100.2;
    private static double sx = 12.5;
    private static double sy = 19.0;
    private static double[] tab = new double[100];
    private static int[] inttab = new int[1000];
    
    /** Creates a new instance of quantify_ex2 */
    public quantify_ex2() {
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        test1_1();
        test1_2();
        test2_1();
        test2_2();
        test3_1();
        test3_2();
        test4_1();
        test4_2();
        System.out.println("Program zakonczyl sie poprawnie!");
    }
    
    //test1
    //stosowanie operacji przesunięcia bitowego zamiast
    //mnozenia lub dzielenia przez liczbe bedaca potega 2
    private static void test1_1() {
        int x, y;
        x = z * 64;
        y = z / 128;
        x = z * 32;
        y = x / 64;
        
    }
    
    private static void test1_2() {
        int x, y;
        x = z << 6;
        y = z >> 7;
        x = z << 5;
        y = z >> 6;
    }
    
    //test2
    //eliminacja wielokrotnego wyliczania wartosci tych samych wyrazen
    private static void test2_1() {     
        double x = d * (lim / max) * sx;
        double y = d * (lim / max) * sy;
        
        for (int i = 0; i < tab.length; i++)
            tab[i] = Math.PI * Math.cos(y);
    }
    
    private static void test2_2() {
        double depth = d * (lim / max);
        double x = depth * sx;
        double y = depth * sy;
        
        double picosy = Math.PI * Math.cos(y);
        for (int i = 0; i < tab.length; i++)
            tab[i] = picosy;     
    }
    
    //test3
    //o ile jest to mozliwe nalezy uzywac final metod/klas
    //zadeklarowanie metody jako final oznacza, ze metoda ta nie bedzie nadpisywana w klasach potomnych
    //zadeklarowanie klasy jako final oznacza, ze po klasie tej nie bedzie sie dziedziczyc
    //wywolywanie metod final jest wydajniejsze
    private static final void test3_1() {
        int x = z++;
    }
    
    private static void test3_2() {
        int x = z++;
    }
    
    //test4
    //stosowanie o ile to mozliwe prostych mechanizmów "systemowych"
    //np. zamiast sprawdzac czy osiagniety zostal koniec tablicy, przechwycenie wyjatku
    private static void test4_1() {
        for (int i = 0; i < inttab.length; i++) { 
            inttab[i] = i; 
        } 
    }
    
    private static void test4_2() {
        try {
            for (int i = 0; ; i++) { 
                inttab[i] = i; 
            } 
        } catch (ArrayIndexOutOfBoundsException exception) {
        } 
    }
}
